$(document).ready(function() {

    /**
     * When the user clicks the button send a DELETE request to the server.  If
     * the server deletes the resource display a success message, if another error
     * occurs display the warning.
     */
    $("#destroy-user").click(function() {
        var id = $("#destroy-user").attr("data-id");
        $.ajax({
            url: "/users/" + id,
            type: "DELETE"
        }).done(function( status ) {
                if (status == "true") {
                    html = '<div class="alert alert-success"><strong>Success:</strong> Account destroyed</div>';
                    $("#messages").append( html );
                } else {
                    html = '<div class="alert alert-warning"><strong>Warning:</strong> Could not find user account</div>';
                    $("#messages").append( html );
                }
        });
    });

    /**
     * Function for handling the login form, get the form data and send it the
     * the RESTful login controller and try to authenticate the user.
     * If the user can"t logged in display and error message.  If the user is
     * logged in redirect them to their intended page.
     */
    function login(){
        var postData = {
            username: $("#username").val(),
            password: $("#password").val()
        };
        $.ajax({
            url: "login",
            type: "POST",
            data: postData
        }).done(function( status ) {
                if (status != "false") {
                    console.log("true");
                    window.location.replace(status);
                } else {
                    html = '<div class="alert alert-danger"><strong>Error:</strong> Username or password are incorrect</div>';
                    $("#username-group").addClass("has-error");
                    $("#password-group").addClass("has-error");
                    $("#messages").append( html );
                    $("#password").attr("disabled", false)
                }
            });

    }
    // if the user clicks the login button on the login form call login()
    $("#btn-login").click(login);
    // if the user hits enter on the password field call login()
    $("#password").on("keypress", function (event) {
        if(event.which == "13"){
            $(this).attr("disabled", true);
            login();
        }
    });

    /**
     * Function for adding an update through AJAX using the resourceful controller.
     */
    // hide the update form and handle showing it
    $("#update-form").hide();
    $("#update-create").click(function() {
        $("#update-form").show();
    });
    $("#update-submit").click(function() {
        var postData = {
            ticket_id: $("#ticket-id").attr("data-id"),
            description: $("#update-field").val()
        };
        $.ajax({
            url: "/updates",
            type: "POST",
            data: postData
        }).done(function( status ) {

                if (status != "false") {
                    $("#updates-none").hide();
                    $("#updates").append("<li>"+ status + "</li>");
                } else {
                    html = '<div class="alert alert-danger"><strong>Error:</strong> Could not add update</div>';
                    $("#update-group").addClass("has-error");
                    $("#password-group").addClass("has-error");
                    $("#messages").append( html );
                }
            });
    });

    /**
     * Function for closing a ticket
     */
    $("#close-ticket").click(function() {
        var id = $(this).attr("data-id");
        $(this).hide();
        $.ajax({
            url: "/tickets/" + id,
            type: "DELETE"
        }).done(function( status ) {

                if (status == "true") {
                    html = '<div class="alert alert-success"><strong>Success:</strong> Ticket closed</div>';
                    $("#messages").append( html );
                } else {
                    html = '<div class="alert alert-warning"><strong>Warning:</strong> Ticket already closed</div>';
                    $("#messages").append( html );
                }
            });
    });

    /**
     * Function for getting the json response from the API for creating an email to send
     * out.  It will open outlook and it will open a new window with the email contents.
     */
    $("#ticket-email").click(function() {
        var id = $(this).attr("data-id");
        $.ajax({
            url: "/api/email/" + id,
            type: "GET"
        }).done(function( email ) {
                window.open(email["url"]);
                document.location.href = email["email"];
            });
    })

    /**
     * Function for getting json response from API for tickets created during the last
     * 7 days
     */
    function getTicketsThisWeek()
    {
        return $.ajax({
            dataType: "json",
            url: "/api/tickets-this-week"
        });
    }

    /**
     * Function for getting json response from API for tickets created during the last
     * 7 days that are currently still open
     */
    function getOpenTicketsThisWeek()
    {
        return $.ajax({
            dataType: "json",
            url: "/api/open-tickets-this-week"
        });
    }

    /**
     * Function for getting json response from API for updates from last 7 days
     */
    function getUpdatesThisWeek()
    {
        return $.ajax({
            dataType: "json",
            url: "/api/updates-this-week"
        });
    }

    /**
     * Function for getting json response from API for activity in the last 7 days
     */
    function getActivityThisWeek()
    {
        return $.ajax({
            dataType: "json",
            url: "/api/activity-this-week"
        });
    }

    /**
     * If the user is on the home page we will want to call the function that control
     * the API and build a graph from that data.
     */
    if ($("#home").length !== 0) {
        // wait for the Ajax data before proceedings
        $.when(getTicketsThisWeek(), getOpenTicketsThisWeek(), getUpdatesThisWeek(),
            getActivityThisWeek()).done(function(tickets, open, updates, activity) {
                /*
                 * Collect all of the ajax and store it into arrays
                 */
                // collect the ticket data into arrays
                var ticketDay = [];
                var ticketCount = [];
                $.each(tickets[0], function() {
                    ticketDay.push(this.day);
                    ticketCount.push(this.count);
                });
                // collect the open ticket data into arrays
                var openDay = [];
                var openCount = [];
                $.each(open[0], function() {
                    openDay.push(this.day);
                    openCount.push(this.count);
                });
                // collect the update data into arrays
                var updateDay = [];
                var updateCount = [];
                $.each(updates[0], function() {
                    updateDay.push(this.day);
                    updateCount.push(this.count);
                });
                // collect the activity into arrays
                var activityDay = [];
                var activityCount = [];
                $.each(activity[0], function() {
                    activityDay.push(this.day);
                    activityCount.push(this.count);
                });
                /*
                 * Organize the data for the graphs from the Ajax data
                 */
                var ticketData = {
                    labels : ticketDay,
                    datasets : [
                        {
                            fillColor : "rgba(220,220,220,0.5)",
                            strokeColor : "rgba(220,220,220,1)",
                            pointColor : "rgba(220,220,220,1)",
                            pointStrokeColor : "#fff",
                            data : ticketCount
                        },
                        {
                            fillColor : "rgba(151,187,205,0.5)",
                            strokeColor : "rgba(151,187,205,1)",
                            pointColor : "rgba(151,187,205,1)",
                            pointStrokeColor : "#fff",
                            data : openCount
                        }
                    ]
                }
                var updateData = {
                    labels : updateDay,
                    datasets : [
                        {
                            fillColor : "rgba(151,187,205,0.5)",
                            strokeColor : "rgba(151,187,205,1)",
                            pointColor : "rgba(151,187,205,1)",
                            pointStrokeColor : "#fff",
                            data : updateCount
                        }
                    ]
                }
                var activityData = {
                    labels : activityDay,
                    datasets : [
                        {
                            fillColor : "rgba(151,187,205,0.5)",
                            strokeColor : "rgba(151,187,205,1)",
                            pointColor : "rgba(151,187,205,1)",
                            pointStrokeColor : "#fff",
                            data : activityCount
                        }
                    ]
                }
                /*
                 * Build the graphs from all of the data
                 */
                var ctxTicket = $("#ticket-chart").get(0).getContext("2d");
                var ticketChart = new Chart(ctxTicket).Line(ticketData);
                var ctxUpdate = $("#update-chart").get(0).getContext("2d");
                var updateChart = new Chart(ctxUpdate).Line(updateData);
                var ctxActivity = $("#activity-chart").get(0).getContext("2d");
                var activityChart = new Chart(ctxActivity).Line(activityData);
            });
    }


    /**
     * jQuery for adding new hubs, locations, and products
     */
    if ($("#settings").length != 0) {

        // dynamically add one of the settings resources based on objName
        function addNewSetting(objName) {
            var data = {}
            $("#" + objName + "-messages").children().remove();
            $("form#new-" + objName + "-form :input").each(function(){
                data[this.name] = this.value;
            });
            $.ajax({
                url: "/" + objName + "s",
                type: "POST",
                data: data
            }).done(function( response ) {
                    if (response.valid) {
                        var success = '<div class="alert alert-success"><strong>Success:</strong> ' + response.message + '</div>';
                        $("#" + objName + "-messages").append(success);
                    } else {
                        console.log(response)
                        var errors = '<div class="panel panel-danger"><div class="panel-heading">' +
                            '<h3 class="panel-title">Error:</h3></div><br><ul>';
                        $.each(response.message, function(){
                            errors += '<li>' + this + '</li>'
                        });
                        errors += '</ul></div>';
                        $("#" + objName + "-messages").append(errors);
                    }
                });
        }

        function fillList(objName) {
            $.ajax({
                url: "/" + objName + "s",
                type: "GET",
                dataType: "json"
            }).done(function( response ) {
                    response = $.parseJSON(response);
                    $.each(response, function(){
                        if (this.name === undefined) {
                            $("#" + objName + "s-list").append(this.title + "<br>");
                        } else {
                            $("#" + objName + "s-list").append(this.name + "<br>");
                        }
                    });
                });
        }

        /**
         * Hide forms on page load
         */
        $("#hub-form").hide();
        $("#location-form").hide();
        $("#product-form").hide();


        /**
         * Listen for button clicks
         */
        $("#new-hub").click(function() {
            $("#hub-form").show();
        });

        $("#new-location").click(function() {
            $("#location-form").show();
        });

        $("#new-product").click(function() {
            $("#product-form").show();
        })

        $("#btn-new-hub").click(function() {
            addNewSetting("hub");
        });

        $("#btn-new-location").click(function() {
            addNewSetting("location");

        });

        $("#btn-new-product").click(function() {
            addNewSetting("product");
        });

        /**
         * Fill the lists with their items
         */
        fillList("hub");
        fillList("location");
        fillList("product");
    }

});