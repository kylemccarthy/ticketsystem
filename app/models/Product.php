<?php

class Product extends Eloquent
{

    protected $table = 'products';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('name', 'impact');

    public function tickets()
    {
        return $this->belongsToMany('Ticket');
    }

}