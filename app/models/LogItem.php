<?php

class LogItem extends Eloquent
{

    protected $table = 'log';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('user_id', 'desc', 'type');

    public function type()
    {
        return $this->hasOne('LogType');
    }

    /**
     * When a new user account is created see what user created it and log that
     * info.
     *
     * @param $userId
     */
    public static function newUser($userId)
    {
        $currentUser = Auth::user();
        $newUser = User::find($userId);
        $desc = $currentUser->first_name . " " . $currentUser->last_name .
            " created an account for " . $newUser->first_name . " " . $newUser->last_name;
        LogItem::create(array(
            'user_id' => $currentUser->id,
            'desc' => $desc,
            'type' => 1,
        ));
    }

    /**
     * When a user account is removed log what account it is and what user removed
     * the account.
     *
     * @param $userId
     */
    public static function removeUser($userId)
    {
        $currentUser = Auth::user();
        $newUser = User::find($userId);
        $desc = $currentUser->first_name . " " . $currentUser->last_name .
            " removed an account for " . $newUser->first_name . " " . $newUser->last_name;
        LogItem::create(array(
            'user_id' => $currentUser->id,
            'desc' => $desc,
            'type' => 1,
        ));
    }

    /**
     * When a new ticket is created log that action and store it in recent activity.
     *
     * @param $ticketId
     */
    public static function newTicket($ticketId)
    {
        $currentUser = Auth::user();
        $newTicket = Ticket::find($ticketId);
        $desc = $currentUser->first_name . " " . $currentUser->last_name . " created ticket "
            . $newTicket->ticket_number;
        LogItem::create(array(
            'user_id' => $currentUser->id,
            'desc' => $desc,
            'type' => 2
        ));
    }

    /**
     * When a ticket is closed log that action and store it in recent activity.
     *
     * @param $ticketId
     */
    public static function destroyTicket($ticketId)
    {
        $currentUser = Auth::user();
        $newTicket = Ticket::find($ticketId);
        $desc = $currentUser->first_name . " " . $currentUser->last_name . " closed ticket "
            . $newTicket->ticket_number;
        LogItem::create(array(
            'user_id' => $currentUser->id,
            'desc' => $desc,
            'type' => 2
        ));
    }

    /**
     * When a user updates a ticket log that action and store it in recent activity.
     *
     * @param $updateId
     */
    public static function newUpdate($updateId)
    {
        $currentUser = Auth::user();
        $update = Update::find($updateId);
        $desc = $currentUser->first_name . " " . $currentUser->last_name . " updated ticket "
            . $update->ticket->ticket_number;
        LogItem::create(array(
            'user_id' => $currentUser->id,
            'desc' => $desc,
            'type' => 3
        ));
    }

}