<?php

class Location extends Eloquent
{

    protected $table = 'locations';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('title', 'city', 'state', 'division', 'hub', 'cfo', 'director');

    public function tickets()
    {
        return $this->belongsToMany('Ticket');
    }

}