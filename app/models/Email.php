<?php

class Email
{
    /**
     * Construct the Email Object
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->ticket = Ticket::findOrFail($id);
        $this->hubs = $this->ticket->hubs;
        $this->locations = $this->ticket->locations;
    }

    /**
     * Get the email to send the email to.
     *
     * @return mixed
     */
    private function mailto()
    {
        $str = "";
        if (count($this->hubs) > 0) {
            foreach ($this->hubs as $hub) {
                $str = $str . $hub->director . "," . $hub->cfo . "," .
                    $hub->division . ",";
            }
        }
        foreach ($this->locations as $location) {
            $str = $str . $location->director . "," . $location->cfo . "," .
            "division" . $location->division . "_vps@chs.net,";
        }
        return str_replace(';', ',', $str);
    }

    /**
     * Return the mailto string while the to field encoded and the subject included
     *
     * @return string
     */
    public function url()
    {
        return "mailto:" . rawurlencode($this->mailto()) . "?subject=". $this->ticket->subject . "&body=";
    }
}