<?php

class LogType extends Eloquent
{

    protected $table = 'log_type';
    public $timestamps = true;
    protected $softDelete = false;

}