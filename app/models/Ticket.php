<?php

class Ticket extends Eloquent
{

    protected $table = 'tickets';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('resolved', 'actions_taken', 'subject', 'ticket_number', 'summary');

    /* ----------------------------------------------------------------------------------
     * RELATIONS
     * ----------------------------------------------------------------------------------
     */

    public function updates()
    {
        return $this->hasMany('Update');
    }

    public function products()
    {
        return $this->belongsToMany('Product');
    }

    public function locations()
    {
        return $this->belongsToMany('Location');
    }

    public function hubs()
    {
        return $this->belongsToMany('Hub');
    }

    /* ----------------------------------------------------------------------------------
     * STATIC FUNCTIONS
     * ----------------------------------------------------------------------------------
     */

    /**
     * Try to create a new ticket resource.  If the ticket is not valid return
     * the validator messages, if the user is valid return the new resource.
     *
     * @param $data
     * @return mixed
     */
    public static function make($data)
    {
        $validate = Ticket::validate($data);
        if ($validate->fails()) {
            return $validate->messages();
        } else {
            $ticket = Ticket::create($data);
            $ticket->save();
            return $ticket;
        }
    }

    /**
     * Validate the ticket on new resource.
     *
     * @param $fields
     * @return mixed
     */
    public static function validate($fields)
    {
        return Validator::make($fields, array(
            'ticket_number' => 'required|unique:tickets',
            'summary'       => 'required',
        ));
    }

}