<?php

class Update extends Eloquent
{

    protected $table = 'updates';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('description', 'ticket_id');

    public function ticket()
    {
        return $this->belongsTo('Ticket');
    }

    /* ----------------------------------------------------------------------------------
     * STATIC FUNCTIONS
     * ----------------------------------------------------------------------------------
     */

    /**
     * Try to create a new update resource.  If the update is not valid return
     * the validator messages, if the user is valid return the new resource.
     *
     * @param $data
     * @return mixed
     */
    public static function make($data)
    {
        $validate = Update::validate($data);
        if ($validate->fails()) {
            return $validate->messages();
        } else {
            $update = Update::create($data);
            $update->save();
            return $update;
        }
    }

    /**
     * Validate the update on new resource.
     *
     * @param $fields
     * @return mixed
     */
    public static function validate($fields)
    {
        return Validator::make($fields, array(
            'description' => 'required',
        ));
    }

}