<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    protected $table = 'users';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('username', 'email', 'first_name', 'last_name');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    // laravel update 4.1.25 -> 4.1.26
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /* ----------------------------------------------------------------------------------
     * RELATIONS
     * ----------------------------------------------------------------------------------
     */

    /**
     * Return the tickets that user user has created.
     *
     * @return mixed
     */
    public function tickets()
    {
        return $this->hasMany('Ticket');
    }

    /**
     * Return the user's activity.
     *
     * @return mixed
     */
    public function log()
    {
        return $this->hasMany('LogItem');
    }


    /* ----------------------------------------------------------------------------------
     * STATIC FUNCTIONS
     * ----------------------------------------------------------------------------------
     */

    /**
     * Try to create a new user resource.  If the user info is not valid return
     * the validator messages, if the user is valid return the new resource.
     *
     * @param $data
     * @return mixed
     */
    public static function make($data)
    {
        $validate = User::validate($data);
        if ($validate->fails()) {
            return $validate->messages();
        } else {
            $user = User::create($data);
            $user->password = Hash::make($data['password']);
            $user->save();
            return $user;
        }
    }

    /**
     * Validate the new user resource.
     *
     * @param $fields
     * @return mixed
     */
    public static function validate($fields)
    {
        return Validator::make($fields, array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'username' => 'required|unique:users',
        ));

    }

    /**
     * Validate the user on and update.
     *
     * @param $fields
     * @return mixed
     */
    public static function updateValidate($fields)
    {
        return Validator::make($fields, array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'min:8',
        ));
    }

}