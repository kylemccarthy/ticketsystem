<?php

class Hub extends Eloquent
{

    protected $table = 'hubs';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('title', 'division', 'hub', 'cfo', 'director');

    public function tickets()
    {
        return $this->belongsToMany('Ticket');
    }

}