<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* --------------------------------------------------------------------------
 * Basic Routes
 * --------------------------------------------------------------------------
 */

Route::get('/', function()
{
    return View::make('pages/home');
});

Route::get('logout', function() {
    Auth::logout();
    return Redirect::to("/");
});
Route::get('settings', array('before' => 'auth', function() {
        return View::make('pages/settings/index');
}));

/* --------------------------------------------------------------------------
 * RESTful Controllers
 * --------------------------------------------------------------------------
 */

Route::controller('login', 'LoginController');
Route::controller('api', 'ApiController');

/*
 * --------------------------------------------------------------------------
 * Resourceful Controllers
 * --------------------------------------------------------------------------
 */

Route::resource('users', 'UserController');
Route::resource('tickets', 'TicketController');
Route::resource('activity', 'LogController');
Route::resource('updates', 'UpdateController');
Route::resource('hubs', 'HubController');
Route::resource('products', 'ProductController');
Route::resource('locations', 'LocationController');
