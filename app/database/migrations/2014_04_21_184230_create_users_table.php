<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('username', 60);
			$table->string('email', 255);
			$table->string('password', 255);
			$table->string('first_name', 100);
			$table->string('last_name', 100);
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}