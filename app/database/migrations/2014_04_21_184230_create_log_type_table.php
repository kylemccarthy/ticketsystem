<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogTypeTable extends Migration {

	public function up()
	{
		Schema::create('log_type', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('desc', 255);
		});
	}

	public function down()
	{
		Schema::drop('log_type');
	}
}