<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration {

	public function up()
	{
		Schema::create('tickets', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->tinyInteger('resolved');
			$table->string('start', 100);
			$table->string('end', 100)->nullable()->default('NULL');
			$table->string('subject', 255);
			$table->text('actions_taken')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('tickets');
	}
}