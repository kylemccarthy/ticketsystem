<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUpdatesTable extends Migration {

	public function up()
	{
		Schema::create('updates', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('description');
		});
	}

	public function down()
	{
		Schema::drop('updates');
	}
}