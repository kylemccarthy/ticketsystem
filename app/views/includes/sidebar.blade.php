<div class="col-sm-3 col-md-2 sidebar">
    @if (Auth::check()) <h3>Welcome {{{ Auth::user()->first_name }}}</h3> @endif
    <ul class="nav nav-sidebar">
        <li><a href="/tickets">Open Tickets</a></li>
        <li><a href="/tickets/create">New Ticket</a></li>
        <li><a href="/users">Users</a></li>
        <li><a href="/users/create">Add user</a></li>
        <li><a href="/activity">Activity</a></li>
    </ul>
</div>