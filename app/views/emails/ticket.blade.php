<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style>
        body {
            font-family: sans-serif;
        }
        h1 {
            font-size: 22px;
        }
        .underline {
            text-decoration: underline;
        }
        ul {
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tr>
        <td align="left" valign="top">
            <table border="0" cellpadding="10" cellspacing="0" width="700">
                <tr>
                    <td align="center" valign="top" colspan="2">
                        <h1>Community Health Systems<br>Information Technology</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong class="underline">Unplanned Outage</strong>
                    </td>
                </tr>
                <tr>
                    <td width="30%"><strong>Reference:</strong></td>
                    <td width="70%">{{{ $ticket->ticket_number }}}</td>
                </tr>
                <tr>
                    <td width="30%"><strong>Summary:</strong></td>
                    <td width="70%">{{{ $ticket->summary }}}</td>
                </tr>
                @if (count($ticket->hubs) > 0)
                <tr>
                    <td width="30%"><strong>Hubs:</strong></td>
                    <td width="70%">
                        <ul>
                            @foreach ($ticket->hubs as $hub)
                            <li>{{{ $hub->title }}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @endif
                @if (count($ticket->locations) > 0)
                <tr>
                    <td width="30%"><strong>Locations:</strong></td>
                    <td width="70%">
                        <ul>
                            @foreach ($ticket->locations as $location)
                            <li>{{{ $location->title }}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @endif
                @if (count($ticket->products) > 0)
                <tr>
                    <td width="30%"><strong>Services Affected:</strong></td>
                    <td width="70%">
                        <ul>
                            @foreach ($ticket->products as $product)
                            <li>{{{ $product->name }}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="30%"><strong>Impact:</strong></td>
                    <td width="70%">
                        <ul>
                            @foreach ($ticket->products as $product)
                            <li>{{{ $product->impact }}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @endif
                @if (count($ticket->updates) > 0)
                <tr>
                    <td width="30%"><strong>Updates:</strong></td>
                    <td width="70%">
                        <ul>
                            @foreach ($ticket->updates as $update)
                            <li>{{{ $update->description }}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @endif
                {{--
                This following piece of code will add two hours onto the time and then it rounds it to the
                nearest 5 minutes.
                --}}
                <tr>
                    <td width="30%"><strong>Next Update:</strong></td>
                    <td width="70%">{{ date("F j, Y - h:i a", round((time() + (60 * 60 * 2))/300)*300) }}</td>
                </tr>
                <tr>
                    <td width="30%"><strong>Start time:</strong></td>
                    <td width="70%">{{{ date("F j, Y - h:i a", strtotime($ticket->created_at)) }}}</td>
                </tr>
                @if ($ticket->resolved)
                <tr>
                    <td width="30%"><strong>End time:</strong></td>
                    <td width="70%">{{{ date("F j, Y - h:i a", strtotime($ticket->updated_at)) }}}</td>
                </tr>
                @endif
                <tr>
                    <td width="30%"><strong>Actions taken by user:</strong></td>
                    <td width="70%">{{{ $ticket->actions_taken }}}</td>
                </tr>
                <tr>
                    <td width="30%"><strong>Getting help:</strong></td>
                    <td width="70%">If you have any questions or concerns regarding this event,
                        please contact the Service Desk at 615-465-3100.</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>