@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1>Add new user</h1>

    {{--
    The form for creating a new user. This form uses Laravel's Blade syntax.  Documentation
    can be found at http://laravel.com/docs/html.
    --}}

    @if ($errors->count() > 0)
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">The following errors have occurred:</h3>
        </div>
        <div class="panel-body">
            <ul>
                @foreach( $errors->all() as $message )
                <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    {{ Form::open(array('action' => 'UserController@store')) }}

    <div class="form-group">
        {{ Form::label('first_name', 'First name') }}
        {{ Form::text('first_name', Input::get('first_name'), array('class' => 'form-control', 'placeholder' => 'first name')) }}
    </div>

    <div class="form-group">
        {{ Form::label('last_name', 'Last Name') }}
        {{ Form::text('last_name', Input::get('last_name'), array('class' => 'form-control', 'placeholder' => 'last name')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email Address') }}
        {{ Form::text('email', Input::get('email'), array('class' => 'form-control', 'placeholder' => 'email address')) }}
    </div>

    <div class="form-group">
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username', Input::get('username'), array('class' => 'form-control', 'placeholder' => 'username')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'password')) }}
    </div>

    {{ Form::submit('Create user', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>

@stop