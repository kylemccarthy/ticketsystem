@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1>All Users</h1>

    <div id="messages"></div>

    <table class="table table-striped">
        <tr>
            <td><strong>ID</strong></td>
            <td><strong>First Name</strong></td>
            <td><strong>Last Name</strong></td>
            <td><strong>Email</strong></td>
            <td><strong>View</strong></td>
        </tr>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->email}}</td>
            <td><a href="/users/{{ $user->id }}" class="btn btn-xs btn-default">View</a></td>
        </tr>
        @endforeach
    </table>

</div>

@stop