@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="row">
        <div class="col-sm-4">
            <h1>User Information</h1>
        </div>
        <div class="col-sm-8 text-right">
            <a href="/users" class="btn btn-default">View All</a>
            <button data-toggle="modal" data-target="#remove-user" class="btn btn-danger">Remove User</button>
            <a href="/users/{{ $user->id }}/edit" class="btn btn-primary">Edit User</a>
        </div>
    </div>

    <div id="messages"></div>

    <table class="table table-striped">
        <tr>
            <td><strong>First:</strong></td>
            <td>{{ $user->first_name }}</td>
        </tr>
        <tr>
            <td><strong>Last:</strong></td>
            <td>{{ $user->last_name }}</td>
        </tr>
        <tr>
            <td><strong>Email:</strong></td>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <td><strong>Username:</strong></td>
            <td>{{ $user->username }}</td>
        </tr>
    </table>
</div>

<div class="modal fade" id="remove-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Remove user?</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove the user?  This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" data-role="remove-user" id="destroy-user" data-id="{{ $user->id }}" data-dismiss="modal">Remove User</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop