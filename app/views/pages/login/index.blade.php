@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="row">
        <div class="col-sm-6">
            <h1>Login</h1>

            {{--
            The form for logging in a user.  This uses AJAX to log the user in, if it fails it
            will display an error and highlight the fields. This form uses Laravel's Blade syntax.
            Documentation can be found at http://laravel.com/docs/html.
            --}}

            <div id="messages"></div>

            {{ Form::open(array('url' => '/login/#', 'id' => 'login-form')) }}

            <div class="form-group" id="username-group">
                {{ Form::label('username', 'Username'); }}
                {{ Form::text('username', Input::get('username'), array('class' => 'form-control', 'placeholder' => 'username')); }}
            </div>

            <div class="form-group" id="password-group">
                {{ Form::label('password', 'Password'); }}
                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'password')); }}
            </div>

            {{ Form::button('Login', array('class' => 'btn btn-primary', 'id' => 'btn-login')); }}

            {{ Form::close() }}
        </div>
    </div>
</div>

@stop