@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="settings">
    <h1>Settings</h1>

    <h2>Hubs <button class="btn btn-sm btn-default glyphicon glyphicon-plus"
                        id="new-hub"></button></h2>
    <div class="row" id="hub-form">
        <div class="row"><div id="hub-messages" class="col-sm-8"></div></div>
        <div class="form-group col-sm-8">
            {{ Form::open(array('url' => '/settings/#', 'id' => 'new-hub-form')) }}

            <div class="form-group">
                {{ Form::label('title', 'Title'); }}
                {{ Form::text('title', null, array('class' => 'form-control')); }}
            </div>

            <div class="form-group">
                {{ Form::label('director', 'Director'); }}
                {{ Form::text('director', null, array('class' => 'form-control',
                'placeholder' => 'CHS ALL CFO s; CHS ALL IS Directors')); }}
            </div>

            <div class="form-group">
                {{ Form::label('cfo', 'CFO'); }}
                {{ Form::text('cfo', null, array('class' => 'form-control',
                'placeholder' => 'AFMC CFO; YoungstownOH CFO')); }}
            </div>

            <div class="form-group">
                {{ Form::label('division', 'Division'); }}
                {{ Form::text('division', null, array('class' => 'form-control',
                'placeholder' => 'example: division1_vps@chs.net; division2_vps@chs.net')); }}
            </div>

            {{ Form::button('Add Hub', array('class' => 'btn btn-primary', 'id' => 'btn-new-hub')); }}

            {{ Form::close() }}
        </div>
    </div>
    <div class="three-cols" id="hubs-list"></div>

    <h2>Locations <button class="btn btn-sm btn-default glyphicon glyphicon-plus"
                     id="new-location"></button></h2>
    <div class="row" id="location-form">
        <div class="form-group col-sm-8" id="location-form">
            <div id="location-form">
                <div class="row"><div id="location-messages" class="col-sm-8"></div></div>
                {{ Form::open(array('url' => '/settings/#', 'id' => 'new-location-form')) }}

                <div class="form-group">
                    {{ Form::label('title', 'Title'); }}
                    {{ Form::text('title', null, array('class' => 'form-control')); }}
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        {{ Form::label('city', 'City'); }}
                        {{ Form::text('city', null, array('class' => 'form-control')); }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::label('state', 'State Code'); }}
                        {{ Form::text('state', null, array('class' => 'form-control',
                        'placeholder' => 'example: MO')); }}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        {{ Form::label('division', 'Division Number'); }}
                        {{ Form::text('division', null, array('class' => 'form-control',
                        'placeholder' => 'example: 5')); }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::label('hub', 'Hub ID'); }}
                        {{ Form::text('hub', null, array('class' => 'form-control', 'placeholder' => 'example: 2')); }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('director', 'Director'); }}
                    {{ Form::text('director', null, array('class' => 'form-control')); }}
                </div>

                <div class="form-group">
                    {{ Form::label('cfo', 'CFO'); }}
                    {{ Form::text('cfo', null, array('class' => 'form-control')); }}
                </div>

                <div class="form-group">
                    {{ Form::label('division', 'Division'); }}
                    {{ Form::text('division', null, array('class' => 'form-control')); }}
                </div>

                {{ Form::button('Add Location', array('class' => 'btn btn-primary', 'id' => 'btn-new-location')); }}

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="three-cols" id="locations-list">

    </div>

    <h2>Products <button class="btn btn-sm btn-default glyphicon glyphicon-plus"
                          id="new-product"></button></h2>
    <div class="row" id="product-form">
        <div class="form-group col-sm-8" id="product-form">
            <div class="row"><div id="product-messages" class="col-sm-8"></div></div>
            {{ Form::open(array('url' => '/settings/#', 'id' => 'new-product-form')) }}

            <div class="form-group">
                {{ Form::label('name', 'Title'); }}
                {{ Form::text('name', null, array('class' => 'form-control')); }}
            </div>

            <div class="form-group">
                {{ Form::label('impact', 'Impact'); }}
                {{ Form::text('impact', null, array('class' => 'form-control')); }}
            </div>

            {{ Form::button('Add Product', array('class' => 'btn btn-primary', 'id' => 'btn-new-product')); }}

            {{ Form::close() }}
        </div>
    </div>
    <div class="three-cols" id="products-list">

    </div>

</div>

@stop