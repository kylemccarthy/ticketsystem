@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="home">
    <h2>Ticket Data</h2>
    <canvas id="ticket-chart" width="700" height="200"></canvas>
    <h2>Update Data</h2>
    <canvas id="update-chart" width="700" height="200"></canvas>
    <h2>Activity Data</h2>
    <canvas id="activity-chart" width="700" height="200"></canvas>
</div>

@stop