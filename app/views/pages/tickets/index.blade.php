@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="row">
        <div class="col-sm-4">
            <h1>Tickets</h1>
        </div>
        <div class="col-sm-8 text-right">
            <a href="/tickets?status=all" class="btn btn-primary">All</a>
            <a href="/tickets?status=open" class="btn btn-success">Open</a>
            <a href="/tickets?status=closed" class="btn btn-danger">Closed</a>
        </div>
    </div>

    <div id="messages"></div>

    <table class="table table-striped">
        <tr>
            <td><strong>Ticket Number</strong></td>
            <td><strong>Created At</strong></td>
            <td><strong>Status</strong></td>
            <td><strong>View</strong></td>
        </tr>
        @foreach ($tickets as $ticket)
        <tr>
            <td>{{ $ticket->ticket_number }}</td>
            <td>{{ $ticket->created_at }}</td>
            <td>
                @if ($ticket->resolved)
                <span class="text-danger">resolved</span>
                @else
                <span class="text-success">open</span>
                @endif
            </td>
            <td><a href="/tickets/{{ $ticket->id }}" class="btn btn-xs btn-default">View</a></td>
        </tr>
        @endforeach
    </table>

    @if ($tickets->count() < 1)
    <p>There are currently no tickets.</p>
    @endif

</div>

@stop