@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1>New Ticket</h1>

    {{--
    The form for creating a new ticket. This form uses Laravel's Blade syntax.  Documentation
    can be found at http://laravel.com/docs/html.
    --}}

    @if ($errors->count() > 0)
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">The following errors have occurred:</h3>
        </div>
        <div class="panel-body">
            <ul>
                @foreach( $errors->all() as $message )
                <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    {{ Form::open(array('action' => 'TicketController@store')) }}

    <div class="row">
        <div class="form-group col-sm-4">
            {{ Form::label('ticket_number', 'Ticket Number') }}
            {{ Form::text('ticket_number', Input::get('ticket_number'), array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('subject', 'Subject') }}
            {{ Form::text('subject', Input::get('subject'), array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-8">
            {{ Form::label('summary', 'Summary') }}
            {{ Form::textarea('summary', Input::get('summary'), array('class' => 'form-control', 'rows' => 3)) }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-8">
            {{ Form::label('actions_taken', 'Actions taken by user') }}
            {{ Form::textarea('actions_taken', Input::get('actions'), array('class' => 'form-control', 'rows' => 3)) }}
        </div>
    </div>

    <h3>Hubs</h3>
    <div class="form-group light-labels">
        @foreach ($hubs as $hub)
        {{ Form::label('hubs[' . $hub->id . ']', $hub->title) }}
        {{ Form::checkbox('hubs[' . $hub->id . ']', $hub->id) }}
        <br>
        @endforeach
    </div>

    <h3>Sites</h3>
    <p class="small">Note: if you have selected hubs you do not need to reselect the individual sites.</p>
    <div class="form-group three-cols light-labels">
        @foreach ($locations as $location)
            {{ Form::label('locations[' . $location->id . ']', $location->title) }}
            {{ Form::checkbox('locations[' . $location->id . ']', $location->id) }}
            <br>
        @endforeach
    </div>

    <h3>Products</h3>
    <div class="form-group three-cols light-labels">
        @foreach ($products as $product)
        {{ Form::label('products[' . $product->id . ']', $product->name) }}
        {{ Form::checkbox('products[' . $product->id . ']', $product->id) }}
        <br>
        @endforeach
    </div>

    {{ Form::submit('Create ticket', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>

@stop