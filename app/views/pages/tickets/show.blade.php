@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div id="ticket-id" data-id="{{ $ticket->id }}"></div>
    <div class="row">
        <div class="col-sm-4">
            <h1>Ticket #{{ $ticket->ticket_number }}</h1>
        </div>
        <div class="col-sm-8 text-right">
            <a href="/tickets" class="btn btn-default">View All</a>
            <button type="button" data-id="{{ $ticket->id }}" class="btn btn-primary" id="ticket-email">Email</button>
            @if (!$ticket->resolved) <button type="button" id="close-ticket" data-id="{{ $ticket->id }}" class="btn btn-danger">Close</button> @endif
        </div>
    </div>
    <div id="messages"></div>

    <p>{{{ $ticket->summary }}}</p>

    @if (count($ticket->hubs) > 0)
    <h2>Hub(s) Affected</h2>
    <ul>
        @foreach ($ticket->hubs as $hub)
        <li>{{{ $hub->title }}}</li>
        @endforeach
    </ul>
    @endif

    @if (count($ticket->locations) > 0)
    <h2>Location(s) Affected</h2>
    <ul>
        @foreach ($ticket->locations as $location)
        <li>{{{ $location->title . ' ' . $location->city . ', ' . $location->state }}}</li>
        @endforeach
    </ul>
    @endif

    @if (count($ticket->products) > 0)
    <h2>Service(s) Affected</h2>
    <ul>
        @foreach ($ticket->products as $product)
        <li>{{{ $product->name }}}</li>
        @endforeach
    </ul>
    @endif

    @if (count($ticket->products) > 0)
    <h2>Impact</h2>
    <ul>
        @foreach ($ticket->products as $product)
        <li>{{{ $product->impact }}}</li>
        @endforeach
    </ul>
    @endif

    <h2>Updates <button class="btn btn-sm btn-default glyphicon glyphicon-plus"
                        id="update-create"></button></h2>
    <ul id="updates">
    @if (count($ticket->updates) > 0)
        @foreach ($ticket->updates as $update)
        <li>{{{ $update->description }}}</li>
        @endforeach
    @else
    </ul>
    <p id="updates-none">no updates...</p>
    @endif
    <div class="row" id="update-form">
        <div class="form-group col-sm-8 update-group">
            <div id="#messanges"></div>
            {{ Form::textarea('update-field', null, array('class' => 'form-control', 'rows' => 2, 'id' => 'update-field')) }}<br>
            <div class="text-right">
                {{ Form::button('Update', array('class' => 'btn btn-primary', 'id' => 'update-submit')) }}
            </div>
        </div>
    </div>

</div>

@stop