@extends('main')

@section('page')
@include('includes/sidebar')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="row">
        <div class="col-sm-4">
            <h1>Recent Activity</h1>
        </div>
    </div>

    <div id="messages"></div>

    <table class="table table-striped">
        <tr>
            <td><strong>Action</strong></td>
        </tr>
        @foreach ($activity as $item)
        <tr>
            <td>{{ $item->desc }}</td>
        </tr>
        @endforeach
    </table>

</div>

@stop