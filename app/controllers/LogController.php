<?php

/**
 * Class UserController
 *
 * This is a resourceful controller for the log model.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class LogController extends BaseController {

    public function index()
    {
        $activity = LogItem::orderBy('created_at', 'des')->take(100)->get();
        return View::make('/pages/logs/index')->withActivity($activity);
    }
}