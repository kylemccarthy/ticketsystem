<?php

/**
 * Class ApiController
 *
 * This is a RESTful controller for the logging user in.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#restful-controllers
 *
 */

class ApiController extends BaseController {

    /**
     * Get data about the number of tickets created during the last 7 days
     *
     * @return string
     */
    public function getTicketsThisWeek()
    {
        $tickets = array();

        for ($i = 0; $i < 8; $i++) {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-' . $i . 'day'));
            $ticket = array(
                'day' => $date->format('D'),
                'count' => Ticket::where('created_at', 'LIKE', '%' . $date->format('Y-m-d') . '%')
                        ->get()->count(),
            );
            array_unshift($tickets, $ticket);
        }

        return json_encode($tickets);
    }

    /**
     * Get data about the number of tickets still opened created during the last 7 days
     *
     * @return string
     */
    public function getOpenTicketsThisWeek()
    {
        $tickets = array();

        for ($i = 0; $i < 8; $i++) {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-' . $i . 'day'));
            $ticket = array(
                'day' => $date->format('D'),
                'count' => Ticket::where('created_at', 'LIKE', '%' . $date->format('Y-m-d') . '%')
                        ->where('resolved', '=', 0)
                        ->get()->count(),
            );
            array_unshift($tickets, $ticket);
        }

        return json_encode($tickets);
    }

    /**
     * Get data about the number of updates created during the last 7 days
     *
     * @return string
     */
    public function getUpdatesThisWeek()
    {
        $updates = array();

        for ($i = 0; $i < 8; $i++) {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-' . $i . 'day'));
            $update = array(
                'day' => $date->format('D'),
                'count' => Update::where('created_at', 'LIKE', '%' . $date->format('Y-m-d') . '%')
                        ->get()->count(),
            );
            array_unshift($updates, $update);
        }

        return json_encode($updates);
    }

    /**
     * Get data about the general activity during the last 7 days
     *
     * @return string
     */
    public function getActivityThisWeek()
    {
        $activitys = array();

        for ($i = 0; $i < 8; $i++) {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-' . $i . 'day'));
            $activity = array(
                'day' => $date->format('D'),
                'count' => LogItem::where('created_at', 'LIKE', '%' . $date->format('Y-m-d') . '%')
                        ->get()->count(),
            );
            array_unshift($activitys, $activity);
        }

        return json_encode($activitys);
    }

    /**
     * Compose an email for a ticket with the subject and to fields filled out and pass the mailto
     * url and the url for the styled email.
     *
     * @param $id
     * @return Email
     */
    public function getEmail($id)
    {
        $email = new Email($id);
        return Response::json(array(
            'email' => $email->url(),
            'url'   => '/api/email-contents/' . $id,
        ));
    }

    /**
     * Open a window with the contents of the email styled in HTML.
     *
     * @param $id
     * @return mixed
     */
    public function getEmailContents($id)
    {
        $ticket = Ticket::findOrFail($id);
        return View::make('emails/ticket')->withTicket($ticket);
    }

}