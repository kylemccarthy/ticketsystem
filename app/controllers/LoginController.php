<?php

/**
 * Class LoginController
 *
 * This is a RESTful controller for the logging user in.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#restful-controllers
 *
 */

class LoginController extends BaseController {

    /**
     * Display the page and form for logging in a user.
     *
     * @return mixed
     */
    public function getIndex()
    {
        return View::make('pages/login/index');
    }


    /**
     * Try to log in a user from the POST data provided.  Data is sent via AJAX
     * and can be looked at in the app.js file.
     *
     * @return string
     */
    public function postIndex()
    {
        if (Auth::attempt(
            array('username' => Input::get('username'),
                'password' => Input::get('password')))) {
                return Session::get('url.intended', url('/'));
        }

        return "false";
    }
}