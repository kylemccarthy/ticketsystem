<?php

/**
 * Class UpdateController
 *
 * This is a resourceful controller for the update model.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class UpdateController extends BaseController {

    /**
     * The user needs to be logged in to interact with the user controller
     * before the route is accessed check to make sure they are logged in
     * by using the filter.  If they are not they will be redirect to the
     * login page with a message warning them.
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Try to create a new update resource and append the relationships with hubs,
     * products, and locations.  If the ticket is invalid redirect to the create
     * page with a message.
     *
     * @return mixed
     */
    public function store()
    {
        $update = Update::make(Input::all());

        if (get_class($update) == "Update") {
            LogItem::newUpdate($update->id);
            return $update->description;
        }
        return "false";
    }

    /**
     * Function to destroy the update resource on a DELETE request.  This will
     * check to make sure that the user trying to make the action is logged
     * in and that the requested update deletion is real.
     *
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        $update = Update::find($id);
        if (get_class($update) == "Update") {
            $update->delete();
            return "true";
        }
        return "false";
    }
}