<?php

/**
 * Class ProductController
 *
 * This is a resourceful controller.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class ProductController extends BaseController {


    /**
     * Return the JSON data for all of the resources.
     *
     * @return mixed
     */
    public function index()
    {
        return Response::json(Product::all()->toJson());
    }

    /**
     * Try to create the new resource and respond according with JSON.
     *
     * @return mixed
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), array(
            'name'      => 'required|max:250',
            'impact'    => 'required|max:250',
        ));
        if ($validator->fails()) {
            return Response::json(array(
                'valid'     => false,
                'message'   => $validator->messages()->toArray(),
                'input'     => Input::all(),
            ));
        } else {
            Product::create(Input::all());
            return Response::json(array(
                'valid'     => true,
                'message'   => 'The hub has been added',
            ));
        }
    }
}