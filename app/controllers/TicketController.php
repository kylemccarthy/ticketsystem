<?php

/**
 * Class TicketController
 *
 * This is a resourceful controller for the ticket model.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class TicketController extends BaseController {

    /**
     * The user needs to be logged in to interact with the user controller
     * before the route is accessed check to make sure they are logged in
     * by using the filter.  If they are not they will be redirect to the
     * login page with a message warning them.
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Allow the user to use GET data to select certain tickets.  They can
     * choose to view all the tickets, open tickets, and closed tickets.
     * This defaults to only open tickets.
     *
     * @return mixed
     */
    public function index()
    {
        if (Input::has('status')) {
            if (Input::get('status') == "all") {
                $tickets = Ticket::all();
            } else if (Input::get('status') == "closed") {
                $tickets = Ticket::where('resolved', '=', 1)->get();
            } else {
                $tickets = Ticket::where('resolved', '=', 0)->get();
            }
        } else {
            $tickets = Ticket::where('resolved', '=', 0)->get();
        }
        return View::make('pages/tickets/index')->withTickets($tickets);
    }

    /**
     * Display the form for ticket creation.
     *
     * @return mixed
     */
    public function create()
    {
        $hubs = Hub::all()->sortBy('title');
        $locations = Location::all()->sortBy('title');
        $products = Product::all()->sortBy('name');
        return View::make('pages/tickets/create')
            ->withHubs($hubs)->withLocations($locations)->withProducts($products);
    }


    /**
     * Try to create a new ticket resource and append the relationships with hubs,
     * products, and locations.  If the ticket is invalid redirect to the create
     * page with a message.
     *
     * @return mixed
     */
    public function store()
    {
        $ticket = Ticket::make(Input::all());

        if (get_class($ticket) == "Ticket") {
            $hubs = Input::get('hubs');
            $locations = Input::get('locations');
            $products = Input::get('products');

            if(is_array($locations)) {
                foreach($locations as $location) {
                    $ticket->locations()->attach($location);
                }
            }

            if(is_array($hubs)) {
                foreach($hubs as $hub) {
                    $ticket->hubs()->attach($hub);
                }
            }

            if(is_array($products)) {
                foreach($products as $product) {
                    $ticket->products()->attach($product);
                }
            }
            LogItem::newTicket($ticket->id);
            return Redirect::to('tickets/' . $ticket->id);
        } else {
            return Redirect::to('tickets/create')->withInput()->withErrors($ticket);
        }
    }

    /**
     * When the function is called try to find the ticket and 404 if the it isn't
     * in the DB.  If the ticket is found return the view with the its info.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $ticket = Ticket::findOrFail($id);
        return View::make('pages/tickets/show')->withTicket($ticket);
    }

    /**
     * Function to destroy the ticket resource on a DELETE request.  This will
     * check to make sure that the user trying to make the action is logged
     * in and that the requested ticket deletion is real.  This doesn't
     * actually remove a ticket from the DB it just changes the resolved to
     * true.
     *
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);
        if (!is_null($ticket) && !$ticket->resolved) {
            LogItem::destroyTicket($ticket->id);
            $ticket->resolved = 1;
            $ticket->save();
            return "true";
        }
        return "false";
    }
}