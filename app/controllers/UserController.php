<?php

/**
 * Class UserController
 *
 * This is a resourceful controller for the users model.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class UserController extends BaseController {

    /**
     * The user needs to be logged in to interact with the user controller
     * before the route is accessed check to make sure they are logged in
     * by using the filter.  If they are not they will be redirect to the
     * login page with a message warning them.
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Show all of the users in the system
     *
     * @return View user index
     */
    public function index()
    {
        $users = User::all();
        return View::make('pages/users/index')->withUsers($users);
    }

    /**
     * Show the form for creating a new user
     *
     * @return View users creation
     */
    public function create()
    {
        return View::make('pages/users/create');
    }

    /**
     * Function for storing a new object.  Check to make sure that all
     * the required fields are present.  If they are create the user and
     * redirect to the resource.  If they required fields are not present
     * return to create page with errors.
     */
    public function store()
    {
        $user = User::make(Input::all());
        if (get_class($user) == "User") {
            LogItem::newUser($user->id);
            return Redirect::to('users/' . $user->id);
        } else {
            return Redirect::to('users/create')->withInput()->withErrors($user);
        }
    }

    /**
     * When the function is called try to find the user and 404 if the user isn't
     * in the DB.  If the user is found return the view with the user info.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return View::make('pages/users/show')->withUser($user);
    }

    /**
     * When the function is called try to find the user and 404 if the user isn't
     * found.  If the user is found return the edit view with the resource data.
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return View::make('pages/users/edit')->withUser($user);
    }

    /**
     * Handle the PUT request for the user.  Check to make sure the user is in
     * the DB and 404 if it isn't.  Then check to make sure that the new info
     * is valid.  If it is update the user and save them and redirect to edit
     * page with a success message.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        $validate = User::updateValidate(Input::all());

        // if the data is invalid redirect with user data and error message
        if ($validate->fails()) {
            return Redirect::to('/users/'. $id . "/edit")->withUser($user)
                ->withErrors($validate);
        }

        $user->first_name   = Input::get('first_name');
        $user->last_name    = Input::get('last_name');
        $user->email        = Input::get('email');
        if (Input::has('password')) {
            $user->password = Input::get('password');
        }
        $user->save();

        return Redirect::to('/users/' . $id . '/edit')->withSuccess(true);

    }

    /**
     * Function to destroy the user resource on a DELETE request.  This will
     * check to make sure that the user trying to make the action is logged
     * in and that the requested user deletion is real.
     *
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (!is_null($user)) {
            LogItem::removeUser($user->id);
            $user->delete();
            return "true";
        }
        return "false";
    }


}