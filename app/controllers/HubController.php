<?php

/**
 * Class HubController
 *
 * This is a resourceful controller.  To learn about how laravel
 * handles resourceful controllers look at: http://laravel.com/docs/controllers#resource-controllers
 *
 */

class HubController extends BaseController {


    /**
     * Return the JSON data for all of the resources.
     *
     * @return mixed
     */
    public function index()
    {
        return Response::json(Hub::all()->toJson());
    }

    /**
     * Try to create the new resource and respond according with JSON.
     *
     * @return mixed
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), array(
            'title'     => 'required|max:250',
            'director'  => 'required',
            'cfo'       => 'required',
            'division'  => 'required',
        ));
        if ($validator->fails()) {
            return Response::json(array(
                'valid' => false,
                'message'   => $validator->messages()->toArray(),
            ));
        } else {
            Hub::create(Input::all());
            return Response::json(array(
                'valid' => true,
                'message'   => 'The hub has been added',
            ));
        }
    }
}